import { Component, OnInit } from '@angular/core';
import '../../../assets/login-animation.js';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { AuthService } from '../../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  signinForm: FormGroup;
  PASSWORD_PATTERN = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{12,}$/;
  
  constructor(public fb: FormBuilder,
    public authService: AuthService,
    public router: Router) {
      this.signinForm = new FormGroup({
        'email': new FormControl('', [
          Validators.required,
          Validators.email
        ]),
        'password': new FormControl('', [
          Validators.required, 
          Validators.pattern(this.PASSWORD_PATTERN)
        ])
      });
  }

  ngOnInit() {
    
  }

  get fc() { return this.signinForm.controls; }

  ngAfterViewInit() {
    (window as any).initialize();
  }

  login(){
    this.authService.signIn(this.signinForm.value);
  }
}
