require('dotenv').config();
console.log("Rest API Server");
const express = require('express'),
      mongoose = require('mongoose'),
      cors = require('cors'),
      bodyParser = require('body-parser'),
      dbConfig = require('./db');

const api  = require('./auth.routes');
const tfa  = require('./tfa');

mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(()=>{
    console.log("Mongodb connected");
}).catch(
    error => {
        console.log("Mongodb can't be connected: " + error)
    }
)

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors());

app.use('/public', express.static('public'));

app.use('/api', api);
app.use(tfa);

// Define PORT
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
    console.log('Restful API started at ' + port)
})

// Express error handling
app.use((req, res, next) => {
    setImmediate(() => {
        next(new Error('Something went wrong'));
    });
});

app.use(function (err, req, res, next) {
    console.error(err.message);
    if (!err.statusCode) err.statusCode = 500;
    res.status(err.statusCode).send(err.message);
});