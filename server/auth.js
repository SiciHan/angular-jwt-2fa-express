// middlewares/auth.js

const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    console.log("auth ..");
    let authErrorMessage = "Invalid authentication, please provide security token!";
    try {
        console.log(req.headers.authorization);
        console.log(req.headers.authorization.includes("Bearer "));
        if(!req.headers.authorization.includes("Bearer ")){
            throw new Error(authErrorMessage);
        }
        const token = req.headers.authorization.split(" ")[1];
        console.log("TOKEN>>> " + token);
        if(token === ''){
            throw new Error(authErrorMessage);
        }
        jwt.verify(token, process.env.JWT_SECRET);
        next();
    } catch (error) {
        console.log(error);
        res.status(401).json({ message: error.message });
    }
};