// routes/auth.routes.js

const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const router = express.Router();
const userSchema = require("./user");
const authorize = require("./auth");
const { check, validationResult } = require('express-validator');

// Sign-up
router.post("/register-user",
    [
        check('firstName')
            .not()
            .isEmpty()
            .isLength({ min: 3 })
            .withMessage('First Name must be atleast 3 characters long'),
        check('lastName')
            .not()
            .isEmpty()
            .isLength({ min: 3 })
            .withMessage('Last Name must be atleast 3 characters long'),
        check('email', 'Email is required')
            .not()
            .isEmpty(),
        check('password', 'Password should be between 5 to 8 characters long')
            .not()
            .isEmpty()
            .isLength({ min: 12, max: 40 })
    ],
    (req, res, next) => {
        const errors = validationResult(req);
        console.log(req.body);

        if (!errors.isEmpty()) {
            return res.status(422).jsonp(errors.array());
        }
        else {
            bcrypt.hash(req.body.password, 10).then((hash) => {
                const user = new userSchema({
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    email: req.body.email,
                    password: hash,
                    secret: '',
                    dataUrl: ''
                });
                user.save().then((response) => {
                    console.log("Register done !");
                    res.status(201).json({
                        message: "User successfully created!",
                        result: response
                    });
                }).catch(error => {
                    res.status(500).json({
                        error: "Error in registration"
                    });
                });
            });
        }
    });


// Sign-in
router.post("/signin", (req, res, next) => {
    let getUser;
    userSchema.findOne({
        email: req.body.email
    }).then(user => {
        if (!user) {
            throw new Error("Authentication failed");
        }
        getUser = user;
        return bcrypt.compare(req.body.password, user.password);
    }).then(response => {
        if (!response) {
            console.log("! response")
            throw new Error("Authentication failed");
        }

        if(typeof(getUser) === 'undefined'){
            console.log("email is undefined...");
            throw new Error("Authentication failed");
        }else{
            console.log("...")
            let jwtToken = jwt.sign({
                email: getUser.email,
                userId: getUser._id,
            }, process.env.JWT_SECRET, {
                expiresIn: "1h"
            });
            res.status(200).json({
                token: jwtToken,
                _id: getUser._id
            });
        }
    }).catch(err => {
        console.log(err);
        return res.status(401).json({
            message: "Authentication failed"
        });
    });
});

// Get Users
router.route('/').get((req, res) => {
    userSchema.find((error, response) => {
        if (error) {
            return next(error)
        } else {
            res.status(200).json(response)
        }
    })
})

// Get Single User
router.route('/user-profile/:id').get(authorize, (req, res, next) => {
    console.log("user profile");
    console.log(req.params.id);
    userSchema.findById(req.params.id, (error, data) => {
        if (error) {
            return next(error);
        } else {
            data.password = "";
            res.status(200).json({
                msg: data
            })
        }
    })
})

// Update User
router.route('/update-user/:id').put(authorize, (req, res, next) => {
    userSchema.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, (error, data) => {
        if (error) {
            return next(error);
            console.log(error)
        } else {
            res.json(data)
            console.log('User successfully updated!')
        }
    });
})


// Delete User
router.route('/delete-user/:id').delete(authorize, (req, res, next) => {
    userSchema.findByIdAndRemove(req.params.id, (error, data) => {
        if (error) {
            return next(error);
        } else {
            res.status(200).json({
                msg: data
            })
        }
    })
})

module.exports = router;